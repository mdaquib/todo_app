const express = require('express')
const tokenVerify = require('../common/tokenVerify')
const logger = require("./handler/error");
const router = express.Router()

//user
router.post('/register', require('./user/register'))
router.post('/login', require('./user/loginLogout').login)
router.post('/logout', tokenVerify, require('./user/loginLogout').logout)
router.get('/profileData', tokenVerify, require('./user/profileData'))
router.get('/getTask', tokenVerify, require('./user/getTask'))

//admin
router.post('/addTask', tokenVerify, require('./admin/addTask'))
router.post('/delete', tokenVerify, require('./admin/deleteTask'))
router.put('/updateTask:taskId', tokenVerify, require('./admin/updateTask'))
router.get('/getAllTask', tokenVerify, require('./admin/getAllTask'))



// Universal Error Handler
router.use((err, req, res, next) => {
    // Log the errors to a file
    logger.error(`${new Date().toISOString()} | ${req.method} - ${req.originalUrl} | Error: ${err.message}`);
    // Send a response
    res.json({
        success: false,
        msg: "Something went wrong, please try again."
    })
})


module.exports = router