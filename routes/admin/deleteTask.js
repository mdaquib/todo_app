const dbTask = require('../../models/addTask')
const dbRegister = require('../../models/register')



module.exports = async(req, res) => {
    try{
        let loginData = await dbRegister.findOne({email: req.decoded.email})
        console.log(loginData)
        if(!loginData || loginData == null){
            res.json({
                success: false,
                msg: 'something went wrong or user not loggedin'
            })
        }else{
            try{
                if(loginData.userType == 'admin'){
                    let deletedData = await dbTask.findByIdAndUpdate({_id: req.body.taskId}, {$set: { status: -1 }})
                    if(deletedData){
                        res.json({
                            success: true,
                            msg: 'task deleted'
                        })
                    }else{
                        res.json({
                            success: false,
                            msg:'something went wrong'
                        })
                    }
                }else{
                    res.json({
                        success: false,
                        msg: "you're not allowed to delete the task"
                    })
                }
            } catch(err){
                next(err)
            }
        }
    }catch(err) {
        next(err)
    }
}