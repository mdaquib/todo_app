const dbtask = require('../../models/addTask')
const dbRegister = require('../../models/addTask')



module.exports = async(req, res) => {
    try{
        let registerData = await dbRegister.findOne({email: req.decoded.email})
        if(!registerData || registerData == null){
            res.json({
                success: false,
                msg: 'user not logged in'
            })
        }else{
            try{
                let taskData = dbtask.findById(req.params.taskId)
                if(!taskData || taskData == null){
                    res.json({
                        success: false,
                        msg: 'no task found'
                    })
                }else{
                    try{
                        if(req.decoded.userType == 'admin'){
                            if(!req.body.task || !req.body.assignedTo){
                                res.json({
                                    success: false,
                                    msg: 'please enter all details'
                                })
                            }else{
                                let taskObj = {
                                    task: req.body.task,
                                    assignedTo: req.body.email,
                                    status: 2
                                }
                                let updateTask = await dbtask.findByIdAndUpdate({_id: req.params.taskId}, { $set: taskObj})
                                if(!updateTask || updateTask == null){
                                    res.json({
                                        success: false,
                                        msg: 'something went wrong'
                                    })
                                }else{
                                    res.json({
                                        success: true,
                                        msg: 'task updated'
                                    })
                                }
                            }
                        }else{
                            res.json({
                                success: false,
                                msg: " you're not allowed to updateTask"
                            })
                        }
                    } catch(err) {
                        next(err)
                    }
                }
            }catch(err) {
                next(err)
            }
        }
    }catch(err) {
        next(err)
    }
}