const dbTask = require('../../models/addTask')
const dbRegister = require('../../models/register')



module.exports = async(req, res) => {
    try{
        let registerData = await dbRegister.findOne({email: req.decoded.email})
        if(!registerData || registerData == null){
            res.json({
                success: false,
                msg: 'user not logged in'
            })
        }else{
            try{
                if(req.decoded.userType == 'admin'){
                    let taskData = await dbTask.find({status: {$ne : -1}})
                        .sort('-createdAt')
                    if(!taskData || taskData == null){
                        res.json({
                            success: false,
                            msg: 'something went wrong'
                        })
                    }else{
                        let taskObj = taskData.map(function(mapped) {
                            console.log(mapped)
                            return {
                                task : mapped.task,
                                assignedTo: mapped.assignedTo,
                                assignedBy: mapped.name,
                                createdAt: mapped.addedAt
                            }
                        })
                        res.json({
                            success: true,
                            msg: 'all task',
                            tasks: taskObj
                        })
                    }
                }
            } catch(err){
                next(err)
            }
        }
    }catch(err){
        next(err)
    }
}