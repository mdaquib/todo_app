const dbTask = require('../../models/addTask')
const dbRegister = require('../../models/register')


module.exports = async(req, res, next) => {
    try{
        let registerData = await dbRegister.findOne({email: req.decoded.email})
        if(!registerData || registerData == null){
            res.json({
                success: false,
                msg: 'user not logged in'
            })
        }else{
            try{
                if(registerData.userType == 'admin'){
                    if(!req.body.task || !req.body.assignedTo){
                        res.json({
                            success: false,
                            msg: 'Please enter all details'
                        })
                    }else{
                        let taskData = await new dbTask({
                            task: req.body.task,
                            name: registerData.name,
                            email: registerData.email,
                            assignedTo: req.body.assignedTo,
                            status: 1
                        })
                        let savedData = await taskData.save()
                        if(savedData){
                            res.json({
                                success: true,
                                msg: 'Task added'
                            })
                        }else{
                            res.json({
                                success: false,
                                msg: 'something went wrong'
                            })
                        }
                    }
                }else{
                    res.json({
                        success: false,
                        msg: "you're not allowed to add task"
                    })
                }
            }catch(err){
                next(err)
            }
        }
    }catch(err) {
        next(err)
    }
}