const dbRegister = require('../../models/register')
const jwt = require('jsonwebtoken')


exports.login = async(req, res, next) => {
    try{
        if(!req.body.email || !req.body.password){
            res.json({
                success: false,
                msg: 'Please enter all details'
            })
        }else{
            let registeredData = await dbRegister.findOne({email: req.body.email})
            if(!registeredData || registeredData == null){
                res.json({
                    success: false,
                    msg: 'user not registered'
                })
            }else{
                try{
                    if(req.body.password == registeredData.password){
                        var tokenData = {
                            email: registeredData.email,
                            phone: registeredData.phone,
                            userType: registeredData.userType
                        }
                    var token = await jwt.sign(tokenData, req.app.get('secretKey'), { expiresIn: '1h' });
                    let login = await dbRegister.findOneAndUpdate({ email: req.body.email }, { $push: { lastLogin: new Date() }, $set: { token: token }})
                        res.json({
                            success: true,
                            msg: 'login successful',
                            token: token
                        })
                    }else{
                        res.json({
                            success: false,
                            msg: 'Incorrect Password'
                        })
                    }
                }catch(err){
                    next(err)
                }
            }
        }
    }catch(err) {
        next(err)
    }
}


//logout
exports.logout = async(req, res, next) =>{
    try{
        let data = await dbRegister.findOneAndUpdate({ email: req.decoded.email}, {$set: {token: null}})
        console.log(data)
        res.json({
            success: true,
            msg: 'loggedOut'
        })
    }catch(err) {
        next(err)
    }
}
