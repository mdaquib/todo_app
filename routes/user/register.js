const dbRegister = require('../../models/register')
const dbProfile = require('../../models/profile')
const dbAdmin = require('../../models/admin')


module.exports = async(req, res, next) => {
    try{
        if(!req.body.name || !req.body.email || !req.body.password || !req.body.age || !req.body.phone || !req.body.userType){
            res.json({
                success: false,
                msg: 'please provide all details'
            })
        }else{
            try{
                let registeredData = await dbRegister.findOne({$or: [{email: req.body.email}, {phone: req.body.phone}]})
                if(registeredData || registeredData != null){
                    res.json({
                        success: false,
                        msg: 'email or phone already registered'
                    })
                }else{
                    try{
                        var regData = await new dbRegister({
                            name: req.body.name,
                            email: req.body.email,
                            password: req.body.password,
                            age: req.body.age,
                            phone: req.body.phone,
                            userType: req.body.userType,
                            status: 0
                        })
                       let data = await regData.save()
                       if(data || data != null){
                            try{
                                var profileData = await new dbProfile({
                                    name: regData.name,
                                    email: regData.email,
                                    age: regData.age,
                                    phone: regData.phone,
                                    userType: regData.userType,
                                    status: 1
                                })
                                let profData = await profileData.save()
                                if(profData && req.body.userType == 'admin'){
                                    try{
                                        var adminData = await new dbAdmin({
                                            name: regData.name,
                                            email: regData.email,
                                            age: regData.age,
                                            phone: regData.phone,
                                            status: 2
                                        })
                                        let adData = await adminData.save()
                                        if(adData || adData != null){
                                            res.json({
                                                success: true,
                                                msg: 'admin registered'
                                            })
                                        }else{
                                            res.json({
                                                success: false,
                                                msg: 'something went wrong'
                                            })
                                        }
                                    }catch(err){
                                        next(err)
                                    }
                                }else{
                                    res.json({
                                        success: true,
                                        msg: 'user registered'
                                    })
                                }
                            }catch(err){
                                next(err)
                            }
                       }
                    }catch(err){
                        next(err)
                    }
                }
            }catch(err){
                next(err)
            }
        }
    } catch(err){
        next(err)
    }
}