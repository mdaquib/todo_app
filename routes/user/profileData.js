const dbProfile = require('../../models/profile')


module.exports = async(req, res, next) => {
    try{
        if(!req.decoded.email){
            res.json({
                success: false,
                msg: 'user not loggedin'
            })
        }else{
            try{
                let profileData = await dbProfile.findOne({email: req.decoded.email})
                if(!profileData || profileData == null){
                    res.json({
                        success: false,
                        msg: 'something went wrong'
                    })
                }else{
                    res.json({
                        success: false,
                        profileData: profileData
                    })
                }
            }catch(err){
                next(err)
            }
        }
    } catch(err){
        next(err)
    }
}