const dbTask = require('../../models/addTask')
const dbRegister = require('../../models/register')


module.exports = async(req, res, next) => {
    try{
        let registeredData = await dbRegister.findOne({email: req.decoded.email})
        if(!registeredData || registeredData == null){
            res.json({
                success: false,
                msg: 'user not logged in'
            })
        }else{
            try{
                let taskData = await dbTask.find({assignedTo: req.decoded.email})
                    .sort('-createdAt')
                if(taskData.length == 0){
                    res.json({
                        success: false,
                        msg:'no task found'
                    })
                }else{
                    res.json({
                        success: true,
                        msg: 'Task assigned to you!',
                        task: taskData

                    })
                }
            } catch(err){
            next(err)
            }
        }
    }catch(err){
        next(err)
    }
}