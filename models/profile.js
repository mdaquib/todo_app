const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * Status
 * 0: User registered
 * 1: Profile created
 * 2: Admin data Updated
 * 3: Profile updated
 **/

profile = new Schema({
    name: String,
    email: String,
    phone: Number,
    age: Number,
    userType: {
        type: String,
        enum: ['admin', 'user']
    },
    status: Number,
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: [Date]
})

module.exports = mongoose.model('profile', profile)