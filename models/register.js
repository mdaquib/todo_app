const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * Status
 * 0: User registered
 * 1: Profile created
 * 2: Admin data Updated
 * 3: Profile updated
 **/

register = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true,
        unique: true
    },
    age: {
        type: Number,
        require: true
    },
    userType: {
        type: String,
        enum: ['admin', 'user']
    },
    token: String,
    status: Number,
    createdAt: {
        type: Date,
        default: new Date()
    },
    lastLogin: [Date]
})

module.exports = mongoose.model('register', register)