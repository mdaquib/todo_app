const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * Status
 *-1: Task deleted
 * 1: Task created
 * 2: Task Updated
 **/

const task = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    task: {
        type: String,
        required: true
    },
    status: Number,
    assignedTo: [String],
    addedAt: {
        type: Date,
        default: new Date()
    }
})


module.exports = mongoose.model('task', task)
