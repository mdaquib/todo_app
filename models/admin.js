const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * Status
 * 0: User registered
 * 1: Profile created
 * 2: Admin data Updated
 * 3: Profile updated
 **/

const admin = new Schema({
    name: String,
    email: String,
    password: String,
    status: Number,
    phone: Number,
    createdAt: {
        type: Date,
        default: new Date()
    }
})


module.exports = mongoose.model('admin', admin)