const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

let token;

const server = require('../server');

// Login the user and store the token for further APIs.
before('/User Login', () => {
    it('it should login the user', (done) => {
        chai.request(server)
            .post('/api/login')
            .send({
                'email': 'saddf@gmail.com',
                'password': 'dsfsefgdg'
            })
            .end((err, res) => {
                token = res.body.token;
                res.body.should.be.a('object');
                res.body.should.have.property('token')
                done();
            });
    });

})

// Hit the API(s) with the details
describe('/Book Order Fail Cases', () => {
    it('it should cause book order failure in Delhivery Surface', (done) => {
        chai.request(server)
            .post('/api/addTask')
            .set('token', token)
            .send({
                "task": "sakjhdkajdla",
                "assignedTo": "sad@gmail.com",
            })
            .end((err, res) => {
                //console.log( new Date(), __filename, res.body);
                expect(res.body).to.have.property('success')
                expect(res.body.success).to.be.true;
                expect(res.body.msg).to.be.an('string')
                done();
            })
    })
})
//     it('it should cause book order failure in Delhivery Express', (done) => {
//         chai.request(server)
//             .post('/user/order/bookOrder')
//             .set('x-access-token', token)
//             .send({
//                 "orderId": "SGMU19100140CSVO10",
//                 "courier": "SGdelhiveryE",
//                 "shippingAmount": 25370
//             })
//             .end((err, res) => {
//                 //console.log( new Date(), __filename, res.body);
//                 expect(res).to.have.status(200);
//                 expect(res.body).to.have.property('success')
//                 expect(res.body.success).to.be.false;
//                 expect(res.body.msg).to.be.an('string')
//                 done();
//             })
//     })
//     it('it should cause book order failure in Delhivery Bulk', (done) => {
//         chai.request(server)
//             .post('/user/order/bookOrder')
//             .set('x-access-token', token)
//             .send({
//                 "orderId": "SGMU19100140CSVO08",
//                 "courier": "SGdelhiveryB",
//                 "shippingAmount": 27140
//             })
//             .end((err, res) => {
//                 //console.log( new Date(), __filename, res.body);
//                 expect(res).to.have.status(200);
//                 expect(res.body).to.have.property('success')
//                 expect(res.body.success).to.be.false;
//                 expect(res.body.msg).to.be.an('string')
//                 done();
//             })
//     })
//     it('it should cause book order failure in Ecom Express', (done) => {
//         chai.request(server)
//             .post('/user/order/bookOrder')
//             .set('x-access-token', token)
//             .send({
//                 "orderId": "SGMU19100140CSVO07",
//                 "courier": "SGecom",
//                 "shippingAmount": 46610
//             })
//             .end((err, res) => {
//                 //console.log( new Date(), __filename, res.body);
//                 expect(res).to.have.status(200);
//                 expect(res.body).to.have.property('success')
//                 expect(res.body.success).to.be.false;
//                 expect(res.body.msg).to.be.an('string')
//                 done();
//             })
//     })
// })