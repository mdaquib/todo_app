const jwt = require('jsonwebtoken')


module.exports = async(req, res, next) => {
    let token = req.headers.token
    if(!token) {
        res.json({
            success: false,
            msg: 'token not found'
        })
    }else{
        jwt.verify(token, req.app.get('secretKey'), (err, decoded) => {
            if(err){
                res.json({
                    success: false,
                    msg: 'something went wrong or token expired'
                })
            }else{
                req.decoded = decoded
                next()
            }
        })
    }
}